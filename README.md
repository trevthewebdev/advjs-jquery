## Advanced Javascript jQuery

### Description
This repository holds all the exercises and project examples for the
Advanced Javascript jQuery class

### Project Setup
Project setup assumes you have node installed on your machine.

`git clone https://gitlab.com/trevthewebdev/advjs-jquery.git`

`npm install`

`npm start`

### Class Link List
Note: You need to have the project running on your local machine for these links to work.

- [Class 1: Exercise 1](http://localhost:3000/class1/ex1/)
- [Class 1: Exercise 2](http://localhost:3000/class1/ex2/)
