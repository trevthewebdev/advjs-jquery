/*
  jQuery Event Delegation
*/

'use strict';

(function($) {
  var itemCount = 0;

  $('button').on('click', function() {
    // Increment the count
    ++itemCount

    // Tell everyone to add the new item
    pubsub.publish('add/item', { item: itemCount });
  });

})(jQuery);
