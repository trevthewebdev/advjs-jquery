/*
  Pubsub Sidebar

  Goals:
  1. Use the pubsub pattern to add an item to the sidebar

*/

'use strict';

(function($) {
  var $sidebarList = $('ul');

  function addItem(index) {
    // Add a new li with the text "item: index"
    var $li = $('<li>');
    $li.text('Item ' + index);
    $li.appendTo($sidebarList);
  }

  // Subscribe to add/item event
  pubsub.subscribe('add/item', function(data) {
    addItem(data.item);
  });

})(jQuery);
