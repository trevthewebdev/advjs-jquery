/*
  jQuery Event Delegation

  Goals:
  Primary Goal: Get used to working with plugins
  1. Configure the dropdown by giving it a list of items to display.
  2. Configure the dropdown to display a custom layout instead of just the label property

*/

'use strict';

(function($) {

  // Your code goes here
  $('.dropdown').DropDown({
    label: 'Board Games',
    listMaxHeight: '250px',
    ajaxFunc: function(resolve) {
      // Make the ajax call to the backend
      $.ajax({ url: '/api/games', dataType: 'json' })
        .done(function(games) {
          // Resolve the promise by returning the list of games
          resolve(
            games.map(function(game) {
              return { label: game.name, value: game.id };
            })
          );
        });
    }
  });

  $('.another-dropdown').DropDown({
    options: [{
      label: 'Something',
      value: 'something'
    }, {
      label: 'Someone',
      value: 'someone'
    }]
  });

})(jQuery);
