/**
 * jQuery Plugins: Custom Dropdown
 */

'use strict';

(function($) {
  function findById(id, obj) {
    if (obj.value === id) {
      return obj;
    }
  };

  // Your code goes here
  var DropDownConstructor = function DropDownConstructor(elem, userOptions) {
    this.config = $.extend({}, this.defaults, userOptions);
    this.$elem = $(elem);

    // Plugin State
    this.isLoading = this.config.isLoading || false;
    this.isOpen = this.isOpen || false;
    this.selected = this.config.selected;
    this.options = this.config.options;
  }

  DropDownConstructor.prototype = {
    defaults: {
      ajaxFunc: null,
      isOpen: false,
      listMaxHeight: '220px',
      options: [],
      selected: {
        label: '-- Select --',
        value: ''
      }
    },

    render: function render() {
      var classes = [
        'dropdown',
        (this.isLoading) ? 'is-loading' : '',
        (this.isOpen) ? 'is-open' : ''
      ];

      // Replace classes on elem
      this.$elem.attr('class', classes.join(' '));

      // Empty the container
      this.$elem.empty();

      // Drop everything into the container
      this.$elem.append([
        (this.config.label) ? $('<label class="input-label">' + this.config.label + '<label>') : '',
        this.buildDropdownDisplay(),
        this.buildDropDownList()
      ]);
    },

    init: function init() {
      var self = this;

      // If user passed an ajax function
      if (typeof this.config.ajaxFunc === 'function') {
        this.isLoading = true;
        var promise = new Promise(function(resolve, reject) {
          self.config.ajaxFunc(resolve, reject);
        });

        promise.then(function(options) {
          self.isLoading = false;
          self.options = options;
          self.render();
        });
      }

      // Immediately try to render the dropdown
      this.render();

      // Bind the events after render
      this.bindEvents();
    },

    bindEvents: function bindEvents() {
      var self = this;

      // Button click
      this.$elem.on('click', '.dropdown-display', function() {
        if (self.isOpen) {
          self.close.call(self);
        } else {
          self.open.call(self);
        }
      });

      this.$elem.on('click', 'li', function() {
        self.selected = self.options.find(findById.bind(null, $(this).data('value')));
        self.isOpen = false;
        self.render();
      });
    },

    buildDropdownDisplay: function buildDropdownDisplay() {
      var loadingText = (this.isLoading) ? 'Loading...' : '';
      var icon = (this.isLoading) ? '<i class="fa fa-spinner fa-pulse">' : '<i class="fa fa-caret-down">';

      return $('<button>')
              .addClass('dropdown-display')
              .data('value', this.selected.value)
              .text(loadingText || this.selected.label)
              .append(icon)
    },

    buildDropDownList: function buildDropDownList() {
      return $('<ul class="dropdown-list">')
               .attr('style', 'max-height:' + this.config.listMaxHeight)
               .append(this.buildDropDownOptions());
    },

    buildDropDownOptions: function buildOptions() {
      return this.options.map(function transformItemsToElements(option) {
        return $('<li>')
                .data('value', option.value)
                .text(option.label);
      });
    },

    open: function open() {
      this.isOpen = true;
      this.render();
    },

    close: function close() {
      this.isOpen = false;
      this.render();
    }
  };

  /* Factory Function */
  $.fn.DropDown = function(userOptions) {
    return this.each(function(index, elem) {
      new DropDownConstructor(this, userOptions).init();
    });
  }

})(jQuery);
