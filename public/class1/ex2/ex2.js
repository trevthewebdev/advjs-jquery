/*
  jQuery Events

  Description:
  The Pointless Button Enabler

  Goals:
  1. When any button is clicked diable it, but enable all others to be clicked.
  2. When clicking on a button apply its corresponding stylesheet so that the page gets updated.

  Hints:
  1. You will need to setup a click with an event handler.
  2. In the click event handler setup previously, you might want to check the html element
     to see if it has any attributes that can help you.

  Related Files:
  - ex2.html
  */

'use strict';

(function($) {

  // Your code goes here
  var $buttons = $('button');

  $buttons.on('click', function() {
    var $this = $(this);
    var fileName = $this.attr('data-stylesheet');
    var $linkTag = $('#theme-stylesheet');

    // Enable / Disabled
    $buttons
      .removeClass('btn--primary')
      .attr('disabled', false);
    $this
      .addClass('btn--primary')
      .attr('disabled', true);

    // Update the stylesheet
    $linkTag.attr('href', '../css' + fileName + '.css');
  });

})(jQuery);
