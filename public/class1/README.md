## Class 1

### Overview
- What is jQuery & how is it different than Javascript?
- What is the DOM?
- How to include / install jQuery
- Selecting & Inspecting Elements
  - Select by tag
  - Select by classname
  - Select by id
  - Select by css selector
- Advanced Selection
  - DOM Traversal
- Events in jQuery
  - Page Load w/ document.ready
  - click
  - scroll
  - submit
  - ...so on
