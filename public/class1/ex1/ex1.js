/*
  jQuery Selectors

  Description:
  The Pointless Button Enabler

  Goals:
  Log all the following to the console with console.log
  1. Select the h1 tag
  2. Select the tertia-text class
  3. Select the wacky button by its data attribute
  4. Select only the second paragraph by using a sibling selector
  5. Select the div with a class of panel by first selecting the h1 and traversing up to it
  6. Select the link tag for our default stylesheet (located in the <head>)

  Related Files:
  - ex1.html
  */

'use strict';

(function($) {

  // Your code goes here
  console.log($('h1'));
  console.log($('.tertia-text'));
  console.log($('[data-stylesheet="wacky"]'));
  console.log($('p').next('p'));
  console.log($('h1').parent('.panel'));
  console.log($('#theme-stylesheet'));

})(jQuery);
