/*
  Validating Input

  Description:
  In this exercise we will be validating user input into a form and handling the submission.

  Goals:
  1. On submit we need to check if each field is has some input in it.
    - If not, we need to show an error state (put a 'is-erroneous' class on the label)
  2. We need to limit the size of message a user can send to 150 characters
    - Every time the user types a character into the message field we need to reduce the displayed character count by 1.
    - In addition we need to make sure that on submission of the form the character count doesn't exceed the maximum

  Related Files:
  - ex2.html
  */

'use strict';

(function($) {
  var $inputs = $('input, textarea');
  var $messageInput = $('[name="message"]');
  var $charLimitText = $('#character-text');
  var MESSAGE_CHAR_LIMIT = 150;
  var charCount = 0;

  // Hide the success message on show
  $('#success-message').hide();

  // Your code goes here
  $('form').on('submit', function(event) {
    event.preventDefault();
    var formHasErrors = false;

    // Loop over the fields and see if they have a value
    $inputs.each(function(index, elem) {
      var $elem = $(elem);
      var $parent = $elem.parent();
      
      if (!$elem.val().length) {
        $parent.addClass('is-erroneous');
        formHasErrors = true;
      } else {
        $parent.removeClass('is-erroneous');
      }
    });

    if (charCount > MESSAGE_CHAR_LIMIT) {
      $messageInput.parent().addClass('is-erroneous');
      formHasErrors = true;
    }

    // If the form does NOT have errors, show success message
    if (!formHasErrors) {
      $('form').hide();
      $('#success-message').show();
    }
  
  });

  function updateVisualCount() {
    $charLimitText.text((MESSAGE_CHAR_LIMIT - charCount) + ' characters remaining');

    if (MESSAGE_CHAR_LIMIT - charCount < 1) {
      $charLimitText.addClass('text-error');
    } else {
      $charLimitText.removeClass('text-error');
    }
  }

  // Message input behavior
  $messageInput.on('keyup', function() {
    // increased the character count
    charCount = $messageInput.val().length;
    
    // Update the text
    updateVisualCount();
  });

})(jQuery);
