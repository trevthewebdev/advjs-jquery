/*
  jQuery Events Practice

  Description:
  Practice with adding events and preventing unwanted default behaviors.

  Goals:
  1. Listen for a click event on the following elements so that on click they print out their tag name
    - <div id="the-div"></div>
    - <button id="the-button"></button>
    - <a id="the-link"></a>
  2. Listen for submit event on the form so that when the submit button is clicked you can console log 'form is being submitted'

  Related Files:
  - ex1.html
  */

'use strict';

(function($) {

  // Your code goes here
<<<<<<< Updated upstream
  $('#the-div, #the-button, #the-link').on('click', function(event) {
=======
  $('#the-div, #the-button, #the-link').on('click', function() {
>>>>>>> Stashed changes
    event.preventDefault();
    console.log('Tag Name: ', this.tagName);
  });

<<<<<<< Updated upstream
  $('form').on('submit', function(event) {
    event.preventDefault();
    console.log('form is being submited');
=======
  $('form').on('submit', function() {
    event.preventDefault();
    console.log('form is being submitted');
>>>>>>> Stashed changes
  });

})(jQuery);
