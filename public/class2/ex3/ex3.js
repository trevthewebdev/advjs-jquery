/*
  jQuery Event Delegation

  Goals:
  1. Use event delegation to add a click event handler to each of the boxes on the page.

  Related Files:
  - ex3.html
  */

'use strict';

(function($) {

  // Your code goes here
  $('ul').on('click', '.panel', function() {
    console.log('I am getting clicked');
  });

})(jQuery);
