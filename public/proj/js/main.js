// Namespace our app
var $modalBackground = $('.modal-background');

var app = {
  showModal: function($modal) {
    $modal.addClass('is-showing');
    $modalBackground.addClass('is-showing');
  },

  hideModal: function($modal) {
    $modal.removeClass('is-showing');
    $modalBackground.removeClass('is-showing');
  }
};

(function($) {

  // Our code here
  var socket = io();

  $.when(
    $.getScript('/proj/js/login.js'),
    $.getScript('/proj/js/listing.js'),
    $.getScript('/proj/js/voting.js')
  ).then(function() {
    // Start the login script
    app.login(socket);

    // Start the listing script
    app.listing(socket);

    // Start the voting script
    app.voting(socket);
  });

})(jQuery);
