
app.login = function(socket) {
    var $modalBackground = $('.modal-background');
    var $loginForm = $('.login-form');
    var $loginBtn = $('.login-form button');
    var $usernameInput = $('[name="username"]');

    // Listen for user input
    $usernameInput.on('input', function() {
        if ($(this).val().length > 3 ) {
            $loginBtn.prop('disabled', false);
        } else {
            $loginBtn.prop('disabled', true);
        }
    });

    // Listen for submit click
    $loginBtn.on('click', function() {
        var $this = $(this);
        $this.addClass('is-loading');

        // Send username to backend
        socket.emit('/server/login/request', {
            username: $usernameInput.val()
        });
    });

    // Listing is ready, so we can send them to listing screen
    pubsub.subscribe('/client/listing/ready', function() {
        $loginBtn.removeClass('is-loading');
        $loginForm.removeClass('is-showing');
        $modalBackground.removeClass('is-showing');
    });
}
