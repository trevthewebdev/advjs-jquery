app.voting = function(socket) {
  var $voteGameList = $('.vote-game-list');
  var $voteModal = $('.active-voting');

  // Render the games for the voting modal
  function populateVoteGameList(gameIds) {
    // Map over the games and return a game that was found by its id
    var games = gameIds.map(function(id) {
      // Loop over the game and return the game that matched it id
      return app.games.find(function(gameObj) {
        if (gameObj.id == id) {
          return gameObj;
        }
      });
    });

    $voteGameList
      .empty()
      .append(
        games.map(function(game) {
          return (
            $('<li>')
              .addClass('gcm-quarter gcl-quarter')
              .data('game-id', game.id)
              .append(
                $('<img>').attr('src', game.img)
              )
          );
        })
      );
  }

  // Wait for the server to tell us that everyone is ready to vote
  socket.on('/server/voting/ready', function(games) {
    var remainingVotes = 3;

    console.log('Games: ', games);

    // Populate voting modal with games
    populateVoteGameList( Object.keys(games) );

    // Show the voting modal
    app.showModal($voteModal);

    // Listen for click event on game during voting
    $voteGameList.on('click', 'li', function() {
      var $this = $(this);
      var gameId = $this.data('game-id');
      var $remainingVotes = $('.remaining-votes li');

      // If we did previously vote on this game
      if ( remainingVotes && !$this[0].hasVote ) {
        // Let us know that this game has been previously voted for
        $this[0].hasVote = true;

        // Tell the server the game voted for
        socket.emit('/server/vote/cast', {
          gameId: gameId,
          weight: remainingVotes
        });

        switch(remainingVotes) {
          case 3:
            $($remainingVotes[0]).addClass('is-used');
          break;

          case 2:
            $($remainingVotes[1]).addClass('is-used');
          break;

          case 1:
            $($remainingVotes[2]).addClass('is-used');
          break;
        }

        // Reduce the amount of remainging votes
        remainingVotes--;
      }

      // If the user has no more remaining votes
      if (!remainingVotes) {
        socket.emit('/server/vote/complete');
      }

    });
  });

  socket.on('/server/voting/complete', function(games) {
    // Populate voting modal with games
    populateVoteGameList( Object.keys(games) );
  });
}
