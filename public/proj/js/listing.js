app.listing = function(socket) {
    var $userList = $('#user-list');
    var $gameList = $('#game-list');
    var $gameSelectionBox = $('.user-selection');
    var $goVoteButton = $('.user-selection ~ .btn--primary');
    var $voteConfirmationModal = $('.modal').not('.login-form, .active-voting'); // Bad selector
    var $activeVotingModal = $('.modal.active-voting');

    // Function to render a list of users
    function populateUserList(users) {
        $userList.empty();
        $userList.append(
            users.map(function(user) {
                var userChipClasses = (user.has_selection) ? 'user-chip media-object mtm has-selection' : 'user-chip media-object mtm';
                return $('<li class="' + userChipClasses +  '">')
                    .append([
                        $('<figure class="media-object__figure mrm">')
                            .append($('<img src="http://placehold.it/30">')),
                        $('<div class="media-object__desc pts">')
                            .text(user.username)
                    ])
            })
        );
    }

    // Function to render the games list
    function populateGameList(games) {
        $gameList.append(
            games.map(function(game) {
                return $('<li>')
                        .addClass('gcm-third gcl-quarter mbl')
                        .data('game-id', game.id)
                        .append([
                            $('<img>')
                                .attr('src', game.img),
                            $('<h4>')
                                .addClass('mts')
                                .text(game.name)
                        ]);
            })
        );
    }

    // Listen for click on an individule game
    $gameList.on('click', 'li', function() {
        var gameId = $(this).data('game-id');
        var game = app.games.find(function(game) {
          if (game.id === gameId) {
            return game;
          }
        });

        // Add the game id to the proceed button
        $.data($goVoteButton, 'gameId', gameId);

        // Add the visuals to the selected game box
        $gameSelectionBox
          .empty()
          .append([
            $('<div>')
              .addClass('media-object mtn')
              .append([
                $('<figure>')
                  .addClass('media-object__figure')
                  .append(
                    $('<img>')
                        .attr('src', game.img)
                        .css('maxWidth', '70px')
                  ),
                $('<div>')
                  .addClass('media-object__desc')
                  .append([
                    $('<h4>')
                      .addClass('tertia-text')
                      .text(game.name),
                    $('<p>')
                      .addClass('mtn')
                      .text('Has been selected')
                  ])
              ])
          ]);

        // If the button is disabled we need to enable it, otherwise don't do anything
        if ($goVoteButton.prop('disabled')) {
          $goVoteButton.prop('disabled', false);
        }
    });

    $goVoteButton.on('click', function() {
      // Show confirmation Modal
      app.showModal($voteConfirmationModal);
    });

    $voteConfirmationModal.on('click', 'button', function() {
      var gameId = $.data($goVoteButton, 'gameId');

      // Close the modal regardless of selection
      app.hideModal($voteConfirmationModal);

      // If user clicked "Yes"...
      if ($(this).hasClass('btn--primary')) {
        // Remove click event handler from game list LIs
        $gameList.off('click', 'li');

        // Add Class to show selection
        $gameSelectionBox.addClass('has-selection');

        // Disabled proceed to vote button
        $goVoteButton.prop('disabled', true);

        // Confirm the selections and tell the server what game was chosen
        socket.emit('/server/selection/confirm', gameId);
      }
    });

    // Tell individule login was successful
    socket.on('/server/login/successful', function(users) {
        populateUserList(users);

        $.ajax({
            url: '/api/games',
            dataType: 'json'
        }).done(function(games) {
            // Add games to the app namespace
            app.games = games;

            // Render the game listing
            populateGameList(games);

            // Let the front-end know the games have loaded
            pubsub.publish('/client/listing/ready');
        });
    });

    // Tell everyone to re-render the user list
    // It was either a login or the user list was updated somehow
    socket.on('/server/users/updated', function(users) {
        populateUserList(users);
    });
}
