/**
 * Pubsub Module
 *
 * Description:
 * Publish / Subscribe (pubsub) is a mechanism that allows some code to publish
 * and event while others subscribe (listen) for that event. When the event occurs
 * those listening can then react to it.
 *
 * Example Usage:
 * Subscribe: var subscription = pubsub.subscribe('/page/load', function(data) { // React to event here // });
 * Publish: pubsub.publish('/page/load', { prop: value, ... });
 *
 */

(function() {
  window.pubsub = (function() {
    var topics = {};
    var hOP = topics.hasOwnProperty;

    return {
      subscribe: function subscribe(topic, listener) {
        // Create the topic's object if not yet created
        if (!hOP.call(topics, topic)) {
          topics[topic] = [];
        }

        // Add the listener to the queue
        var index = topics[topic].push(listener) -1;

        // Provide handle back for removal of topic
        return {
          remove: function remove() {
            delete topics[topic][index];
          }
        };
      },
      publish: function publish(topic, info) {
        // If the topic doesn't exist, or there's no listeners in queue, just leave
        if (!hOP.call(topics, topic)) {
          return
        }

        // Cycle through each listener in the topic's queue
        topics[topic].forEach(function cycleTopics(item) {
          item(info !== undefined ? info : {});
        });
      }
    }
  })();
})();
