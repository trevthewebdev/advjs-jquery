var id = 0;

function generateId() {
  return ++id;
}

module.exports = [
  {
    id: generateId(),
    name: 'Above and Below',
    img: '/assets/imgs/above-below.jpg'
  },
  {
    id: generateId(),
    name: 'Evolution',
    img: '/assets/imgs/evolution.jpg'
  },
  {
    id: generateId(),
    name: 'Five Tribes',
    img: '/assets/imgs/five-tribes.jpg'
  },
  {
    id: generateId(),
    name: 'The Ancient World',
    img: '/assets/imgs/ancient-world.jpg'
  },
  {
    id: generateId(),
    name: 'Catacombs',
    img: '/assets/imgs/catacombs.jpg'
  },
  {
    id: generateId(),
    name: 'Potion Explosion',
    img: '/assets/imgs/potion-explosion.jpg'
  },
  {
    id: generateId(),
    name: 'Mexica',
    img: '/assets/imgs/mexica.jpg'
  },
  {
    id: generateId(),
    name: 'The Manhattan Project',
    img: '/assets/imgs/m-project.jpg'
  },
  {
    id: generateId(),
    name: 'Rum and Bones',
    img: '/assets/imgs/rum-n-bones.jpg'
  },
  {
    id: generateId(),
    name: 'Orlèans',
    img: '/assets/imgs/orleans.jpg'
  }
];
