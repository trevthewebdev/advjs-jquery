var games = require('./data/games');

exports.getGames = function(req, res) {
  setTimeout(function() {
    res.status(200).json(games);
  }, Math.floor(Math.random() * 2000) + 200 );
}
