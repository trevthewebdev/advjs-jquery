var express = require('express');
var controller = require('./controller');
var Router = express.Router();

Router.get('/games', controller.getGames);
// Router.get('/users', controller.getUsers);

module.exports = Router;
