/*
 Title: App

 Description:
 This file is not intended to be part of the jQuery class but to merely
 run a lightweight server for us to use in class.
*/

var express = require('express');
var app = express();
var server = require('http').createServer(app);
var io = require('socket.io')(server);

app.use(express.static('public'));
app.use('/api', require('./api/routes'));

var users = [];
var games = {};

io.on('connection', function(socket) {
  // When a user requests a login
  socket.on('/server/login/request', function(userData) {
    var user = {
      id: socket.id,
      username: userData.username,
      has_selection: false,
      vote_complete: false
    };

    // Add user to users array
    users.push(user);

    // Add userData to socket for easy access
    socket.userData = user;

    // Tell the user that they are successfully logged in
    socket.emit('/server/login/successful', users);
    io.emit('/server/users/updated', users);
  });

  socket.on('/server/selection/confirm', function(gameId) {
    var votingReady = false;

    // Update userData on the socket
    socket.userData.has_selection = true;

    // Loop over and update the user requesting the confirmation
    users.map(function(user) {
      // Is this the user confirming the selection
      if (user.id === socket.userData.id) {
        user.has_selection = true;
      }

      return user;
    });

    // Add the game to the games object for future voting
    // games is an Object {} NOT an Array
    games[gameId] = 0;

    // Tell everyone that a user has confirmed their game
    io.emit('/server/users/updated', users);

    // Loop over the users and check if everyone has a selection
    for(var i = 0; i < users.length; i++) {
      // If a user does NOT have a selection the set votingReady to false and break the loop
      if (!users[i].has_selection) {
        votingReady = false;
        break;
      } else {
        votingReady = true;
      }
    }

    // If all users have selected a game, tell everyone voting is ready to start
    if (votingReady) {
      io.emit('/server/voting/ready', games);
    }
  });

  socket.on('/server/vote/cast', function(vote) {
    games[vote.gameId] += parseInt(vote.weight);
  });

  socket.on('/server/vote/complete', function() {
    var votingComplete = false;
    var gameIds = [];

    // Loop over and update the user requesting the confirmation
    users.map(function(user) {
      // Is this the user confirming the selection
      if (user.id === socket.userData.id) {
        user.vote_complete = true;
      }

      return user;
    });

    // Loop over the users and check if everyone has a selection
    for(var i = 0; i < users.length; i++) {
      // If a user does NOT have a selection the set votingReady to false and break the loop
      if (!users[i].vote_complete) {
        votingComplete = false;
        break;
      } else {
        votingComplete = true;
      }
    }

    // If all users have selected a game, tell everyone voting is ready to start
    if (votingComplete) {
      //
      var highestValue;
      var highest = {};

      for (var key in games) {
        if (games.hasOwnProperty(key)) {
          if (highestValue) {
            if (games[key] > highestValue) {
              highest = {};
              highestValue = games[key];
              highest[key] = games[key];
            }
          } else {
            highestValue = games[key];
            highest[key] = games[key];
          }
        }
      }

      console.log(highest);

      io.emit('/server/voting/complete', highest);

      // Reset the games object
      games = {};
    }
  });

  // When an individule user disconnects
  socket.on('disconnect', function() {
    if (socket.userData) {
      // Initialize with a negative for a later check
      var indexToRemove = -1;

      // Find user's index in user array
      for (var i = 0; i < users.length; i++) {
        var user = users[i];
        if (user.id === socket.userData.id) {
          indexToRemove = i;
          break;
        }
      }

      // Remove the user from users array
      if (indexToRemove !== -1) {
        users.splice(indexToRemove, 1);
      }

      // Tell everyone about the user leaving
      io.emit('/server/users/updated', users);
    }
  });
});

server.listen(3000, function() {
  console.log('Server listening on: http://localhost:3000');
});
